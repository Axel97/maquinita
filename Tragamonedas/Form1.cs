﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tragamonedas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        double credito= 0;
        double jugado = 0;

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnJugar_Click(object sender, EventArgs e)
        {
            if (credito >= 0.5)
            {
                jugado = jugado + 0.5;
                credito = credito - 0.5;
                txtJugado.Text = jugado.ToString();
                txtCredito.Text = credito.ToString();
            }
            else
            {
                if (credito == 0)
                    MessageBox.Show("No tienes money my friend!!");
            }
            
        }

        private void btnCents_Click(object sender, EventArgs e)
        {
            credito = credito + 0.5;
            txtCredito.Text = credito.ToString(); 
        }

        private void txtCredito_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnUno_Click(object sender, EventArgs e)
        {
            credito = credito + 1;
            txtCredito.Text = credito.ToString();
        }

        private void btnDos_Click(object sender, EventArgs e)
        {
            credito = credito + 2;
            txtCredito.Text = credito.ToString();
        }

        private void btnCobrar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Has ganado: " + credito+" Has jugado: " + jugado);
        }
    }
}
